﻿using System;
using System.Collections.Generic;

namespace ColorBump.Bindings
{
    public static class Binder
    {
        private static readonly Dictionary<Type, IBindConfig> _binds;

        static Binder()
        {
            _binds = new Dictionary<Type, IBindConfig>();
        }

        public static void Bind<T>(IBindConfig config)
        {
            Bind(typeof(T), config);
        }

        public static void Bind(Type type, IBindConfig config)
        {
            _binds.Add(type, config);
        }

        public static void Unbind<T>()
        {
            Unbind(typeof(T));
        }

        public static void Unbind(Type type)
        {
            _binds.Remove(type);
        }

        public static T GetInstance<T>()
        {
            IBindConfig config;
            if (_binds.TryGetValue(typeof(T), out config))
            {
                IBindConfig<T> concreteConfig = config as IBindConfig<T>;
                if (concreteConfig != null)
                    return concreteConfig.GetInstance();
                return (T)config.GetInstance();
            }
            return default(T);
        }

        public static object GetInstance(Type type)
        {
            IBindConfig config;
            if (_binds.TryGetValue(type, out config))
                return config.GetInstance();
            return null;
        }
    }
}