﻿namespace ColorBump.Entities
{
    public enum EntityType
    {
        None,
        White,
        Black,
        Finish
    }
}