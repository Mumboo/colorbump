﻿using System;
using UnityEngine;

namespace ColorBump.Levels
{
    [Serializable]
    public struct PatternContainer
    {
        [SerializeField]
        private GameObject _prefab;
        public GameObject Prefab { get { return _prefab; } }

        [SerializeField]
        private int _weight;
        public int Weight { get { return _weight; } }

        [SerializeField]
        private float _size;
        public float Size { get { return _size; } }
    }
}