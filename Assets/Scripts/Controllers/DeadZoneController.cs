﻿using UnityEngine;

namespace ColorBump.Controllers
{
    public sealed class DeadZoneController : MonoBehaviour
    {
        private void OnCollisionEnter(Collision collision)
        {
            Destroy(collision.gameObject);
        }
    }
}