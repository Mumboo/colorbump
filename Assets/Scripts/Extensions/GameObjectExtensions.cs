﻿using ColorBump.Entities;
using UnityEngine;

namespace ColorBump.Extensions
{
    public static class GameObjectExtensions
    {
        public static bool IsEnemy(this GameObject source, GameObject target)
        {
            EntityType sourceType = source.GetEntityType();
            EntityType targetType = target.GetEntityType();
            return sourceType != EntityType.None && targetType != EntityType.None && sourceType != targetType;
        }

        public static bool IsAlly(this GameObject source, GameObject target)
        {
            EntityType sourceType = source.GetEntityType();
            EntityType targetType = target.GetEntityType();
            return sourceType != EntityType.None && targetType != EntityType.None && sourceType == targetType;
        }

        public static EntityType GetEntityType(this GameObject gameObject)
        {
            Entity entity = gameObject.GetComponent<Entity>();
            return entity != null ? entity.Type : EntityType.None;
        }
    }
}