﻿using ColorBump.Events;
using UnityEngine;

namespace ColorBump.Commands
{
    public sealed class GameCommandsMediator : MonoBehaviour
    {
        private void OnEnable()
        {
            Dispatcher.Subscribe<GameStateChangedEvent>(OnGameStateChanged);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<GameStateChangedEvent>(OnGameStateChanged);
        }

        private void OnGameStateChanged(object sender, GameStateChangedEvent args)
        {
            if (args.State != Matches.GameState.InProgress)
                Command.Execute<LoadMenuSceneCommand>();
        }
    }
}