﻿using ColorBump.Bindings;
using ColorBump.Entities;
using ColorBump.Events;
using ColorBump.Extensions;
using UnityEngine;

namespace ColorBump.Controllers
{
    [RequireComponent(typeof(Rigidbody))]
    public sealed class BallController : MonoBehaviour
    {
        private Vector3 _originPosition;
        public Vector3 OriginPosition { get { return _originPosition; } }

        private GameController GameController { get { return Binder.GetInstance<GameController>(); } }

        [SerializeField]
        private float _movementSpeed;

        [SerializeField]
        private float _relativeSpeed;

        [SerializeField]
        private Vector2 _boundsCenter;
        [SerializeField]
        private Vector2 _boundsSize;

        private Vector3 _relativePosition;

        private Rigidbody _rigidbody;
        private Plane _plane;
        private Vector3 _sourceMousePosition;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _plane = new Plane(Vector3.up, Vector3.zero);
        }

        private void Start()
        {
            Dispatcher.Dispatch<BallInitializedEvent>(this, new BallInitializedEvent(gameObject));
        }

        private void Update()
        {
            if (GameController.State == Matches.GameState.InProgress)
            {
                _originPosition.z += Time.deltaTime * _movementSpeed;
                if (Input.GetMouseButtonDown(0))
                    _sourceMousePosition = Input.mousePosition;
                else if (Input.GetMouseButton(0))
                {
                    Vector3 offset = GetHitPosition(Input.mousePosition) - GetHitPosition(_sourceMousePosition);
                    _sourceMousePosition = Input.mousePosition;
                    _relativePosition += offset * _relativeSpeed;
                }
                Vector3 finalPosition = ClampPosition(_relativePosition + _originPosition);
                _rigidbody.MovePosition(finalPosition);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (GameController.State == Matches.GameState.InProgress)
            {
                if (gameObject.IsEnemy(collision.gameObject))
                    Dispatcher.Dispatch<BallDeadEvent>(this, new BallDeadEvent(gameObject));
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            EntityType type = other.gameObject.GetEntityType();
            if (type == EntityType.Finish)
                Dispatcher.Dispatch<BallReachFinishEvent>(this, new BallReachFinishEvent(gameObject));
        }

        private Vector3 GetHitPosition(Vector3 inputPosition)
        {
            Ray ray = Camera.main.ScreenPointToRay(inputPosition);
            float distance;
            if (_plane.Raycast(ray, out distance))
                return ray.GetPoint(distance);
            return Vector3.zero;
        }

        private Vector3 ClampPosition(Vector3 position)
        {
            Vector3 center = _originPosition + new Vector3(_boundsCenter.x, 0, _boundsCenter.y);
            position.x = Mathf.Clamp(position.x, center.x - _boundsSize.x / 2, center.x + _boundsSize.x / 2);
            position.z = Mathf.Clamp(position.z, center.z - _boundsSize.y / 2, center.z + _boundsSize.y / 2);
            return position;
        }
    }
}