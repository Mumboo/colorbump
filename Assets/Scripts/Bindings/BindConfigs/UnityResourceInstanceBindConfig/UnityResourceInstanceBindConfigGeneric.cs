﻿using UnityEngine;

namespace ColorBump.Bindings
{
    public sealed class UnityResourceInstanceBindConfig<T> : IBindConfig<T> where T : Object
    {
        private readonly string _path;
        private T _instance;

        public UnityResourceInstanceBindConfig(string path)
        {
            _path = path;
        }

        public T GetInstance()
        {
            if (_instance == null)
                _instance = Resources.Load<T>(_path);
            return _instance;
        }

        object IBindConfig.GetInstance()
        {
            return GetInstance();
        }
    }
}