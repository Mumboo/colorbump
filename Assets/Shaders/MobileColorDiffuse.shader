﻿Shader "Mobile/Diffuse" {
Properties {
	_Color ("Color", Color) = (1, 1, 1, 1)
    _MainTex ("Base (RGB)", 2D) = "white" {}
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd

sampler2D _MainTex;
float4 _Color;

struct Input {
    float2 uv_MainTex;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 color = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	color = pow(color, 1.8);
    o.Albedo = color.rgb;
    o.Alpha = color.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}