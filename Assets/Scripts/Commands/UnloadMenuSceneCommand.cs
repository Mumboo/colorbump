﻿using UnityEngine.SceneManagement;

namespace ColorBump.Commands
{
    public sealed class UnloadMenuSceneCommand : Command
    {
        public override void Execute()
        {
            Scene scene = SceneManager.GetSceneByName("Menu");
            if (scene.isLoaded)
                SceneManager.UnloadSceneAsync(scene);
        }
    }
}