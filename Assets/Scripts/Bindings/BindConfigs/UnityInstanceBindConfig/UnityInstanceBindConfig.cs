﻿using System;
using Object = UnityEngine.Object;

namespace ColorBump.Bindings
{
    public sealed class UnityInstanceBindConfig : IBindConfig
    {
        private readonly Type _type;
        private object _instance;

        public UnityInstanceBindConfig(Type type)
        {
            _type = type;
        }

        public object GetInstance()
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType(_type);
            return _instance;
        }
    }
}