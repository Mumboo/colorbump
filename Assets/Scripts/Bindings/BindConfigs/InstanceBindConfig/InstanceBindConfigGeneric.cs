﻿namespace ColorBump.Bindings
{
    public sealed class InstanceBindConfig<T> : IBindConfig<T>
    {
        private readonly T _instance;

        public InstanceBindConfig(T instance)
        {
            _instance = instance;
        }

        public T GetInstance()
        {
            return _instance;
        }

        object IBindConfig.GetInstance()
        {
            return GetInstance();
        }
    }
}