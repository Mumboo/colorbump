﻿using ColorBump.Commands;
using UnityEngine;

namespace ColorBump.Views
{
    public sealed class MenuView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _settingsMenu;

        public void Play()
        {
            Command.Execute<LoadGameSceneCommand>();
        }

        public void Settings()
        {
            _settingsMenu.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}