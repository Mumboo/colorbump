﻿using System;

namespace ColorBump.Bindings
{
    public sealed class LazyInstanceBindConfig<T> : IBindConfig<T>
    {
        private T _instance;

        public T GetInstance()
        {
            if (_instance == null)
                _instance = Activator.CreateInstance<T>();
            return _instance;
        }

        object IBindConfig.GetInstance()
        {
            return GetInstance();
        }
    }
}