﻿using UnityEngine;

namespace ColorBump.Bindings
{
    public sealed class UnityInstanceBindConfig<T> : IBindConfig<T> where T : Object
    {
        private T _instance;

        public T GetInstance()
        {
            if (_instance == null)
                _instance = Object.FindObjectOfType<T>();
            return _instance;
        }

        object IBindConfig.GetInstance()
        {
            return GetInstance();
        }
    }
}