﻿using System;
using UnityEngine;

namespace ColorBump.Events
{
    public sealed class BallDeadEvent : EventArgs
    {
        public readonly GameObject Ball;

        public BallDeadEvent(GameObject ball)
        {
            Ball = ball;
        }
    }
}