﻿using ColorBump.Controllers;
using ColorBump.Services;
using UnityEngine;

namespace ColorBump.Bindings
{
    public static class DefaultBindings
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Bind()
        {
            Binder.Bind<GameController>(new UnityInstanceBindConfig<GameController>());
            BindAudioService();
        }

        private static void BindAudioService()
        {
            AudioService audioService = Resources.Load<AudioService>("AudioService");
            audioService = AudioService.Instantiate(audioService);
            Binder.Bind<AudioService>(new InstanceBindConfig(audioService)); 
        }
    }
}