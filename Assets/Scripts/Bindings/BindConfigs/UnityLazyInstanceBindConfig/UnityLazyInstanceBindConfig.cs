﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ColorBump.Bindings
{
    public sealed class UnityLazyInstanceBindConfig : IBindConfig
    {
        private readonly Type _type;
        private object _instance;

        public UnityLazyInstanceBindConfig(Type type)
        {
            _type = type;
        }

        public object GetInstance()
        {
            if (_instance == null)
            {
                _instance = Object.FindObjectOfType(_type);
                if (_instance == null)
                {
                    GameObject instanceObject = new GameObject(_type.Name, _type);
                    _instance = instanceObject.GetComponent(_type);
                }
            }
            return _instance;
        }
    }
}