﻿using UnityEngine;

namespace ColorBump.Services
{
    [RequireComponent(typeof(AudioSource))]
    public sealed class AudioService : MonoBehaviour
    {
        public bool Mute
        {
            get { return _audio.mute; }
            set { _audio.mute = value; }
        }

        private AudioSource _audio;

        private void Awake()
        {
            DontDestroyOnLoad(this);
            _audio = GetComponent<AudioSource>();
        }
    }
}