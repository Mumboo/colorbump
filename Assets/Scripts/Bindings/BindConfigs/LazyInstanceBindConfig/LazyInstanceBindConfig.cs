﻿using System;

namespace ColorBump.Bindings
{
    public sealed class LazyInstanceBindConfig : IBindConfig
    {
        private readonly Type _type;
        private object _instance;

        public LazyInstanceBindConfig(Type type)
        {
            _type = type;
        }

        public object GetInstance()
        {
            if (_instance == null)
                _instance = Activator.CreateInstance(_type);
            return _instance;
        }
    }
}