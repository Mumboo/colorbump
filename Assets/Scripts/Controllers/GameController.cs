﻿using ColorBump.Events;
using ColorBump.Matches;
using UnityEngine;

namespace ColorBump.Controllers
{
    public sealed class GameController : MonoBehaviour
    {
        private GameState _state;
        public GameState State
        {
            get { return _state; }
            private set
            {
                _state = value;
                Dispatcher.Dispatch<GameStateChangedEvent>(this, new GameStateChangedEvent(_state));
            }
        }

        private void OnEnable()
        {
            Dispatcher.Subscribe<BallDeadEvent>(OnBallDead);
            Dispatcher.Subscribe<BallReachFinishEvent>(OnBallReachFinish);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<BallDeadEvent>(OnBallDead);
            Dispatcher.Unsubscribe<BallReachFinishEvent>(OnBallReachFinish);
        }

        private void OnBallDead(object sender, BallDeadEvent args)
        {
            State = GameState.Defeat;
        }

        private void OnBallReachFinish(object sender, BallReachFinishEvent args)
        {
            State = GameState.Victory;
        }
    }
}
