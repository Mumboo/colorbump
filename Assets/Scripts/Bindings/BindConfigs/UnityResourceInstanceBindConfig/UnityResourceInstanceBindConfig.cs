﻿using System;
using UnityEngine;

namespace ColorBump.Bindings
{
    public sealed class UnityResourceInstanceBindConfig : IBindConfig
    {
        private readonly Type _type;
        private readonly string _path;
        private object _instance;

        public UnityResourceInstanceBindConfig(Type type, string path)
        {
            _type = type;
            _path = path;
        }

        public object GetInstance()
        {
            if (_instance == null)
                _instance = Resources.Load(_path, _type);
            return _instance;
        }
    }
}