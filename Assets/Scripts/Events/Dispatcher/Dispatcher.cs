﻿using System;
using System.Collections.Generic;

namespace ColorBump.Events
{
    public static class Dispatcher
    {
        private static readonly Dictionary<Type, Delegate> _handlers;

        static Dispatcher()
        {
            _handlers = new Dictionary<Type, Delegate>();
        }

        public static void Subscribe<T>(EventHandler<T> callback) where T : EventArgs
        {
            Subscribe(typeof(T), callback);
        }

        public static void Subscribe(Type type, Delegate callback)
        {
            Delegate handler;
            _handlers.TryGetValue(type, out handler);
            handler = Delegate.Combine(handler, callback);
            _handlers[type] = handler;
        }

        public static void Unsubscribe<T>(EventHandler<T> callback) where T : EventArgs
        {
            Unsubscribe(typeof(T), callback);
        }

        public static void Unsubscribe(Type type, Delegate callback)
        {
            Delegate handler;
            if (_handlers.TryGetValue(type, out handler))
            {
                handler = Delegate.Remove(handler, callback);
                if (handler != null)
                    _handlers[type] = handler;
                else
                    _handlers.Remove(type);
            }
        }

        public static void Dispatch<T>(object sender, T args) where T : EventArgs
        {
            Delegate handler;
            if (_handlers.TryGetValue(typeof(T), out handler))
            {
                EventHandler<T> concreteHandler = handler as EventHandler<T>;
                if (concreteHandler != null)
                    concreteHandler.Invoke(sender, args);
                else
                    handler.DynamicInvoke(sender, args);
            }
        }

        public static void Dispatch(Type type, params object[] args)
        {
            Delegate handler;
            if (_handlers.TryGetValue(type, out handler))
                handler.DynamicInvoke(args);
        }
    }
}