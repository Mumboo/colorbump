﻿using ColorBump.Events;
using UnityEngine;

namespace ColorBump.Controllers
{
    public sealed class CameraController : MonoBehaviour
    {
        [SerializeField]
        private Vector3 _offset;

        private BallController _ball;

        private void OnEnable()
        {
            Dispatcher.Subscribe<BallInitializedEvent>(OnBallInitialized);
        }

        private void OnDisable()
        {
            Dispatcher.Unsubscribe<BallInitializedEvent>(OnBallInitialized);
        }

        private void LateUpdate()
        {
            if (_ball != null)
                transform.position = _ball.OriginPosition + _offset;
        }

        private void OnBallInitialized(object sender, BallInitializedEvent args)
        {
            _ball = args.Ball.GetComponent<BallController>();
        }
    }
}