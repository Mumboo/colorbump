﻿using UnityEngine.SceneManagement;

namespace ColorBump.Commands
{
    public sealed class LoadGameSceneCommand : Command
    {
        public override void Execute()
        {
            SceneManager.LoadSceneAsync("Game");
        }
    }
}