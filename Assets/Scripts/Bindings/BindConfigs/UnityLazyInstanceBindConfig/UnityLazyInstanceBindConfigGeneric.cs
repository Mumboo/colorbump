﻿using UnityEngine;

namespace ColorBump.Bindings
{
    public sealed class UnityLazyInstanceBindConfig<T> : IBindConfig<T> where T : Object
    {
        private T _instance;

        public T GetInstance()
        {
            if (_instance == null)
            {
                _instance = Object.FindObjectOfType<T>();
                if (_instance == null)
                {
                    GameObject instanceObject = new GameObject(typeof(T).Name, typeof(T));
                    _instance = instanceObject.GetComponent<T>();
                }
            }
            return _instance;
        }

        object IBindConfig.GetInstance()
        {
            return GetInstance();
        }
    }
}