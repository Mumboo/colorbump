﻿using System;
using UnityEngine;

namespace ColorBump.Events
{
    public sealed class BallReachFinishEvent : EventArgs
    {
        public readonly GameObject Ball;

        public BallReachFinishEvent(GameObject ball)
        {
            Ball = ball;
        }
    }
}