﻿namespace ColorBump.Matches
{
    public enum GameState
    {
        InProgress,
        Victory,
        Defeat
    }
}