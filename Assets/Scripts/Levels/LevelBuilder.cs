﻿using UnityEngine;

namespace ColorBump.Levels
{
    public sealed class LevelBuilder : MonoBehaviour
    {
        [SerializeField]
        private int _patternsCount;

        [SerializeField]
        private PatternContainer[] _patterns;

        [SerializeField]
        private GameObject _finish;

        private void Awake()
        {
            float distance = 10;
            for (int i = 0; i < _patternsCount; i++)
            {
                PatternContainer pattern = GetRandomPattern();
                if (pattern.Prefab != null)
                {
                    GameObject.Instantiate(pattern.Prefab, Vector3.forward * distance, Quaternion.identity, transform);
                    distance += pattern.Size;
                }
            }
            GameObject.Instantiate(_finish, Vector3.forward * distance, Quaternion.identity, transform);
        }

        private PatternContainer GetRandomPattern()
        {
            int weightSum = 0;
            for (int i = 0; i < _patterns.Length; i++)
                weightSum += _patterns[i].Weight;

            int number = Random.Range(0, weightSum + 1);
            for (int i = 0; i < _patterns.Length; i++)
            {
                if (number <= _patterns[i].Weight)
                    return _patterns[i];
                number -= _patterns[i].Weight;
            }
            return default(PatternContainer);
        }
    }
}