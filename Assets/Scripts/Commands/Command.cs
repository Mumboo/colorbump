﻿namespace ColorBump.Commands
{
    public abstract class Command
    {
        public abstract void Execute();

        public static void Execute<T>() where T : Command, new()
        {
            T command = new T();
            Execute<T>(command);
        }

        public static void Execute<T>(T command) where T : Command
        {
            command.Execute();
        }
    }
}