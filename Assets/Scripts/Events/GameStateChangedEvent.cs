﻿using System;
using ColorBump.Matches;

namespace ColorBump.Events
{
    public sealed class GameStateChangedEvent : EventArgs
    {
        public readonly GameState State;

        public GameStateChangedEvent(GameState state)
        {
            State = state;
        }
    }
}