﻿namespace ColorBump.Bindings
{
    public sealed class InstanceBindConfig : IBindConfig
    {
        private readonly object _instance;

        public InstanceBindConfig(object instance)
        {
            _instance = instance;
        }

        public object GetInstance()
        {
            return _instance;
        }
    }
}