﻿using System;
using UnityEngine;

namespace ColorBump.Events
{
    public sealed class BallInitializedEvent : EventArgs
    {
        public readonly GameObject Ball;

        public BallInitializedEvent(GameObject ball)
        {
            Ball = ball;
        }
    }
}