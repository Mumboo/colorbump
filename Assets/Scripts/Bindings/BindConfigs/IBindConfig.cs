﻿namespace ColorBump.Bindings
{
    public interface IBindConfig
    {
        object GetInstance();
    }
}