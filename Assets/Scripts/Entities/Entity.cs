﻿using UnityEngine;

namespace ColorBump.Entities
{
    public sealed class Entity : MonoBehaviour
    {
        [SerializeField]
        private EntityType _type;
        public EntityType Type { get { return _type; } }
    }
}