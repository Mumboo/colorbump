﻿using ColorBump.Bindings;
using ColorBump.Services;
using UnityEngine;
using UnityEngine.UI;

namespace ColorBump.Views
{
    public sealed class SettingsView : MonoBehaviour
    {
        private AudioService AudioService { get { return Binder.GetInstance<AudioService>(); } }

        [SerializeField]
        private GameObject _mainMenu;

        [SerializeField]
        private Toggle _audioOnToggle;

        [SerializeField]
        private Toggle _audioOffToggle;

        private void Awake()
        {
            _audioOnToggle.isOn = !AudioService.Mute;
            _audioOffToggle.isOn = AudioService.Mute;
        }

        private void OnEnable()
        {
            _audioOnToggle.onValueChanged.AddListener(SetMusicState);
        }

        private void OnDisable()
        {
            _audioOnToggle.onValueChanged.RemoveListener(SetMusicState);
        }

        public void Back()
        {
            _mainMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        public void SetMusicState(bool state)
        {
            AudioService.Mute = !state;
        }
    }
}