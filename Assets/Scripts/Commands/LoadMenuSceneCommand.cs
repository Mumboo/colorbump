﻿using UnityEngine.SceneManagement;

namespace ColorBump.Commands
{
    public sealed class LoadMenuSceneCommand : Command
    {
        public override void Execute()
        {
            Scene scene = SceneManager.GetSceneByName("Menu");
            if (!scene.isLoaded)
                SceneManager.LoadSceneAsync("Menu", LoadSceneMode.Additive);
        }
    }
}